# Scholar

This is aLpha and didou's project to learn about web development and project management, made with vue-cli3 and laravel7

## Instalation

### The Backend (Laravel API)

1. Download the repository or clone the repository
```
git clone https://gitlab.com/mahdimedrhm/scholar.git
```

2. Change the directory into api folder
```
cd api/
```

3. Install the dependencies by running Composer's install command
```
composer install
```
4. Create an environment file
```
cp .env.example .env
```
5. Create a database named `scholar
6. Edit `.env` and set your database connection details
```
DB_CONNECTION=mysql
DB_HOST=(127.0.0.1 if not modified)
DB_PORT=port (if using xampp mysql port)
DB_DATABASE=scholar
DB_USERNAME=root
DB_PASSWORD=
```
7. Migrate your database
```
php artisan migrate
```
8. run the seeds :
```
php artisan db:seed
```
### The frontend (vue-cli)
1. Change the directory into api folder
```
cd spa/
```
2. Get the node_modules:
```
yarn install or npm install
```

## Development

1. Run the local server:
```
cd api/
php artisan serve
```
2. Now click on the link shown in the terminal.

3. Vue:
```
cd spa/
yarn serve
```
4. Click on the link shown in the terminal.
